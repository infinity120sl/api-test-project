<?php

require_once "vendor/autoload.php";

try {
    $app = new \App\Api\Core\Application();
    echo $app->run();
} catch (Throwable $e) {
    echo $e->getMessage();
}