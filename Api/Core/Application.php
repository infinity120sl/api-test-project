<?php

declare(strict_types=1);

namespace App\Api\Core;

use App\Api\Core\Database\PDO;
use Exception;

/**
 * Class Application
 *
 * @author Roman Zablodskyi <r.zablodskyi@gmail.com>
 *
 */
class Application
{
    /**
     * @var string
     */
    protected $method = '';

    /**
     * @var string
     */
    protected $path = '';

    /**
     * @var string[]
     */
    protected $allowedMethods = ['GET', 'POST', 'PUT', 'DELETE'];

    /**
     * @var array
     */
    protected $requestParams = [];

    /**
     * @var array
     */
    protected $headers = [];

    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];
        $this->path = trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
        $this->requestParams = $this->prepareParams();
        $this->headers = getallheaders();
    }

    /**
     * @return string
     *
     * @throws Exception
     */
    public function run(): string
    {
        if (in_array($this->method, $this->allowedMethods)) {
            $url = explode('/', $this->path);

            if (count($url) == 2 && array_shift($url) == 'api') {
                $controllerName = $this->getController(array_pop($url));
                $actionName = $this->getAction($controllerName);
                $params = $this->prepareParams();

                $result = (new $controllerName())->$actionName($params);

                $status = 200;
                if (isset($result['status'])) {
                    $status = $result['status'];
                    unset($result['status']);
                }

                return $this->response($result, $status);
            } else {
                throw new Exception('Incorrect api call');
            }
        } else {
            throw new Exception('Not allowed method type');
        }
    }

    /**
     * @return array
     */
    protected function prepareParams(): array
    {
        $result = [
            'headers' => $this->headers,
            'params' => []
        ];

        if ($this->method == 'GET') {
            $result['params'] = $_GET;
        } elseif ($this->method == 'POST') {
            $result['params'] = $_POST;
        }

        return $result;
    }

    /**
     * @param string $name
     *
     * @return string|null
     * @throws Exception
     */
    protected function getController(string $name): ?string
    {
        if (!empty($name)) {
            $controller = "App\\Api\\Controller\\" . ucfirst($name) . 'Controller';

            return class_exists($controller) ? $controller : null;
        } else {
            throw new Exception('Not found controller class');
        }
    }

    /**
     * @param string $className
     *
     * @return string|null
     * @throws Exception
     */
    protected function getAction(string $className): string
    {
        $action = 'action' . ucfirst(strtolower($this->method));

        if (method_exists($className, $action)) {
            return $action;
        } else {
            throw new Exception('Not found action');
        }
    }

    /**
     * @param array $data
     * @param int $status
     *
     * @return string
     */
    protected function response(array $data, int $status): string
    {
        header("Content-Type: application/json");
        header("HTTP/1.1 {$status} {$this->getStatus($status)}");

        return json_encode($data);
    }

    /**
     * @param int $statusCode
     *
     * @return string
     */
    protected function getStatus(int $statusCode): string
    {
        $statuses = [
            '200' => 'OK',
            '401' => 'Unauthorized',
            '500' => 'Internal Server Error'
        ];

        return isset($statuses[$statusCode]) ? $statuses[$statusCode] : $statuses[500];
    }
}