<?php

declare(strict_types=1);

namespace App\Api\Core\Controller;

/**
 * Class Base
 *
 * @author Roman Zablodskyi <r.zablodskyi@gmail.com>
 */
abstract class Base
{
    /**
     * @param array $data
     *
     * @return array
     */
    abstract function actionGet(array $data = []): array;

    /**
     * @param array $data
     *
     * @return array
     */
    abstract function actionPost(array $data = []): array;

    /**
     * @param array $data
     *
     * @return array
     */
    abstract function actionPut(array $data = []): array;

    /**
     * @param array $data
     *
     * @return array
     */
    abstract function actionDelete(array $data = []): array;
}