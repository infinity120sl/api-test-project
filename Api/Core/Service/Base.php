<?php

declare(strict_types=1);

namespace App\Api\Core\Service;

use App\Api\Core\Database\PDO;

/**
 * Class Base
 *
 * @author Roman Zablodskyi <r.zablodskyi@gmail.com>
 */
class Base
{
    /**
     * @return PDO
     */
    protected function getPDO(): PDO
    {
        return new PDO();
    }

    /**
     * @param string $header
     *
     * @return string
     */
    protected function prepareAuthToken(string $header): string
    {
        preg_match("/[0-9a-z]{32}$/", $header, $match);

        return array_pop($match);
    }

    /**
     * @return string
     */
    protected function generateHash(): string
    {
        return md5(uniqid());
    }
}