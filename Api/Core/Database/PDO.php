<?php

declare(strict_types=1);

namespace App\Api\Core\Database;

/**
 * Class PDO
 *
 * @author Roman Zablodskyi <r.zablodskyi@gmail.com>
 */
class PDO
{
    /**
     * @var \PDO|null
     */
    private $pdo = null;

    /**
     * @var string
     */
    private $host = '127.0.0.1';

    /**
     * @var string
     */
    private $db = 'test';

    /**
     * @var string
     */
    private $charset = 'utf8';

    /**
     * @var string
     */
    private $user = 'root';

    /**
     * @var string
     */
    private $password = '';

    public function __construct()
    {
        try {
            $this->pdo = new \PDO(
                "mysql:host={$this->host};dbname={$this->db};charset={$this->charset};",
                $this->user,
                $this->password
            );
        } catch (\Throwable $exception) {
            echo $exception->getMessage();
        }
    }

    /**
     * @param string $sql
     * @param array $params
     *
     * @return \PDOStatement
     */
    public function query(string $sql, array $params = [])
    {
        $smtm = $this->pdo->prepare($sql);

        $smtm->execute($params);

        return $smtm;
    }
}