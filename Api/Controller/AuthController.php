<?php

declare(strict_types=1);

namespace App\Api\Controller;

use App\Api\Core\Controller\Base;
use App\Api\Service\Auth;

/**
 * Class AuthController
 *
 * @author Roman Zablodskyi <r.zablodskyi@gmail.com>
 */
class AuthController extends Base
{
    /**
     * @param array $data
     *
     * @return array
     */
    public function actionPost(array $data = []): array
    {
        $service = new Auth();

        return $service->auth($data);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function actionGet(array $data = []): array
    {
        return [];
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function actionPut(array $data = []): array
    {
        return [];
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public function actionDelete(array $data = []): array
    {
        return [];
    }
}