<?php


namespace App\Api\Controller;


use App\Api\Core\Controller\Base;
use App\Api\Service\Retrieve;

/**
 * Class RetrieveController
 *
 * @author Roman Zablodskyi <r.zablodskyi@gmail.com>
 */
class RetrieveController extends Base
{
    /**
     * @param array $params
     *
     * @return array
     */
    public function actionGet(array $data = []): array
    {
        $service = new Retrieve();

        return $service->retrieve($data);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    function actionPost(array $data = []): array
    {
        return [];
    }

    /**
     * @param array $data
     *
     * @return array
     */
    function actionPut(array $data = []): array
    {
        return [];
    }

    /**
     * @param array $data
     *
     * @return array
     */
    function actionDelete(array $data = []): array
    {
        return [];
    }
}