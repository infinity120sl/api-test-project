<?php

declare(strict_types=1);

namespace App\Api\Controller;

use App\Api\Core\Controller\Base;
use App\Api\Service\Generation;

/**
 * Class GenerateController
 *
 * @author Roman Zablodskyi <r.zablodskyi@gmail.com>
 */
class GenerateController extends Base
{
    /**
     * @param array $data
     *
     * @return array
     *
     * @throws \Exception
     */
    function actionGet(array $data = []): array
    {
        $service = new Generation();

        return $service->generate($data);
    }

    /**
     * @param array $data
     *
     * @return array
     */
    function actionPost(array $data = []): array
    {
        return [];
    }

    /**
     * @param array $data
     *
     * @return array
     */
    function actionPut(array $data = []): array
    {
        return [];
    }

    /**
     * @param array $data
     *
     * @return array
     */
    function actionDelete(array $data = []): array
    {
        return [];
    }
}