<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\Core\Service\Base;

/**
 * Class Generation
 *
 * @author Roman Zablodskyi <r.zablodskyi@gmail.com>
 */
class Generation extends Base
{
    protected const MIN = 1;

    protected const MAX = 100000;

    /**
     * @param array $data
     *
     * @return array
     *
     * @throws \Exception
     */
    public function generate(array $data): array
    {
        $result = [];

        if (isset($data['headers']['Authorization'])
            && $authToken = $this->prepareAuthToken($data['headers']['Authorization'])) {
                $value = rand(self::MIN, self::MAX);
                $pdo = $this->getPDO();

                $authTokenData = $pdo->query("SELECT id, created_at FROM auth_token WHERE token=:token", [
                    'token' => $authToken
                ])->fetch(\PDO::FETCH_ASSOC);

                if (!empty($authTokenData)) {
                    if ($this->getDateTimeDiff(new \DateTime($authTokenData['created_at'])) < 60) {
                        $id = $this->generateHash();
                        $pdo->query("INSERT INTO generation SET id=:id, value=:value, auth_token_id=:auth_token_id", [
                            'id' => $id,
                            'value' => (int)$value,
                            'auth_token_id' => (int)$authTokenData['id']
                        ]);

                        $result = [
                            'id' => $id,
                            'value' => $value
                        ];
                    } else {
                        $result['status'] = 401;
                    }
                }
        }

        return $result;
    }

    /**
     * @param \DateTime $dateTime
     *
     * @return int
     */
    protected function getDateTimeDiff(\DateTime $dateTime): int
    {
        return (new \DateTime())->getTimestamp() - $dateTime->getTimestamp();
    }
}