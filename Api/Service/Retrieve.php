<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\Core\Service\Base;

/**
 * Class Retrieve
 *
 * @author Roman Zablodskyi <r.zablodskyi@gmail.com>
 */
class Retrieve extends Base
{
    public function retrieve(array $data): array
    {
        $result = [];

        if (isset($data['params']['id']) && isset($data['headers']['Authorization'])) {
            $authToken = $this->prepareAuthToken($data['headers']['Authorization']);
            $value = $this->getPDO()->query("SELECT g.value 
                                    FROM generation AS g
                                    INNER JOIN auth_token at
                                        ON g.auth_token_id = at.id
                                    WHERE g.id=:id AND at.token=:token", [
                'id' => $data['params']['id'],
                'token' => $authToken
            ])->fetch(\PDO::FETCH_ASSOC);

            if (!empty($value)) {
                $result['value'] = $value['value'];
            }
        }

        return $result;
    }
}