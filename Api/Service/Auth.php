<?php

declare(strict_types=1);

namespace App\Api\Service;

use App\Api\Core\Service\Base;

/**
 * Class Auth
 *
 * @author Roman Zablodskyi <r.zablodskyi@gmail.com>
 */
class Auth extends Base
{
    /**
     * @param array $data
     *
     * @return array
     */
    public function auth(array $data): array
    {
        $result = [];
        $params = $data['params'];

        if (isset($params['login']) && isset($params['pass'])) {
            $token = $this->generateHash();
            $pdo = $this->getPDO();

            $userId = $pdo->query("SELECT id FROM user WHERE login=:login AND pass=:pass", [
                'login' => $params['login'],
                'pass' => $params['pass']
            ])->fetch(\PDO::FETCH_ASSOC);

            if (!empty($userId)) {
                $pdo->query('INSERT INTO auth_token SET token=:token, created_at=:created_at, user_id=:user_id', [
                    'token' => $token,
                    'created_at' => (new \DateTime())->format('Y-m-d H:i:s'),
                    'user_id' => $userId['id']
                ]);

                $result['token'] = $token;
            }
        }

        return $result;
    }
}